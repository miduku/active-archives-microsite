// const baseURL_OnlineDir = 'https://interface.fh-potsdam.de/active-archives/'
// const baseURL_OnlineDir = '/active-archives/'
const baseURL_OnlineDir = '/'
const baseURL = process.env.NODE_ENV === 'dev' ? '/' : baseURL_OnlineDir
const onlineURL = 'https://test.dustinkummer.com/active-archives'
// const onlineURL = 'https://interface.fh-potsdam.de/active-archives'

const pkg = require('./package')

module.exports = {
  mode: 'universal',

  router: {
    base: baseURL,
  },

  /*
  ** Headers of the page
  */
  head: {
    title: `${process.env.NODE_ENV === 'dev' ? '(' + process.env.NODE_ENV + ') ' : ''}Active Archives – Fachhochschule Potsdam | University of Applied Sciences`,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ]/* ,
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ] */
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#333' },

  /*
  ** Global CSS
  */
  css: [
    '~/assets/css/index.scss'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/vue-scrollactive',
    { src: "~/plugins/vue-resize-directive", ssr: false },
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    ['nuxt-sass-resources-loader', '~/assets/css/index.scss'],

    // Doc: https://github.com/nuxt-community/axios-module#usage
    // '@nuxtjs/axios',

    // Doc: https://github.com/nuxt-community/nuxt-i18n/tree/master/docs
    ['nuxt-i18n', {
      baseURL: onlineURL,
      vueI18nLoader: true,
      lazy: true,
      langDir: 'locales/',
      locales: [
        {
          code: 'en',
          iso: 'en-US',
          file: 'en-US.js'
        },
        {
          code: 'de',
          iso: 'de-DE',
          file: 'de-DE.js'
        },
      ],
      defaultLocale: 'de',
      detectBrowserLanguage: {
        cookieKey: 'aa_language_detection'
      }
    }]
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    // baseURL:
    //   process.env.NODE_ENV === "production" ? baseURL : "http://localhost:3000"
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
          options: {
            fix: true
          }
        })
      }
    }
  }
}
