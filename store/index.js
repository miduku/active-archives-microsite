// export default createStore

export const state = () => ({
  isNavOpen: false,
  anchors: [],
  sizes: {
    wWidth: 0,
    wHeight: 0
  }
})

export const mutations = {
  SET_ANCHORS (state, payload) {
    state.anchors = payload
  },
  SET_SIZES (state, payload) {
    state.sizes = payload
  },
  SET_NAV_STATE (state, payload) {
    state.isNavOpen = payload
  }
}
