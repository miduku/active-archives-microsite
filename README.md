# Active Archives Microsite

> Microsite for the course “Active Archives” at the University of Applied Sciences

## Build Setup

``` bash
# Use Node v9.11.1
# Check .nvmrc

# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
