export default {
  methods: {
    createAnchors () {
      const sections = this.$el.querySelectorAll('.section')
      const ids = []

      sections.forEach(section => {
        ids.push(section.id)
      })

      this.storeAnchors(ids)
    },

    storeAnchors (anchors) {
      this.$store.commit('SET_ANCHORS', anchors)
    }
  }
}
