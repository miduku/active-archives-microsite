export default {
  default: {
    intro: 'Einleitung',
    projects: 'Projekte',
    legal: 'Impressum',
    links: 'Links',
    semester: 'Semester',
    projFrom: 'Projekt von',
    website: 'Website',
    documentation: 'Dokumentation'
  }
}
