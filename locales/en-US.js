export default {
  default: {
    intro: 'Introduction',
    projects: 'Projects',
    legal: 'Legal',
    links: 'Links',
    semester: 'Semester',
    projFrom: 'Project from',
    website: 'Website',
    documentation: 'Documentation'
  }
}
